from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

import hello.views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gettingstarted.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', hello.views.shop, name='shop'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^',include('shopify_auth.urls')),
)

# Initialize Shopify Authorization
import shopify_auth.apps
shopify_auth.apps.initialise_shopify_session()
