import logging
logger = logging.getLogger(__name__)

from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse

from shopify_auth.decorators import login_required

import shopify

# Create your views here.
@login_required
def shop(request):
    with request.user.session:
        shop_url = request.user.myshopify_domain
        shop_name = shopify.Shop.current().name
    
    logger.info("Show shop: " + shop_url)
    
    return render(request, "shop.html", {
        'shop_url': shop_url,
        'shop_name': shop_name,
        'shopify_api_key': settings.SHOPIFY_APP_API_KEY
    })
