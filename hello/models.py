from django.db import models
from shopify_auth.models import AbstractShopUser

# Create your models here.
class ShopUser(AbstractShopUser):
    # Derived model
    pass
