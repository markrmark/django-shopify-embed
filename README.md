# django-shopify-embed

This is based on the python-getting-started Django demo app for Heroku, with added functionality from DiscoLabs django-shopify-auth repository that allows for integration with the Shopify Embedded App SDK.

A barebones Python Django App which integrates with the Shopify Embedded App SDK. It can easily be deployed to Heroku.

References:

This application support the [Getting Started with Python on Heroku](https://devcenter.heroku.com/articles/getting-started-with-python) article - check it out.

[Code for django-shopify-auth](https://github.com/discolabs/django-shopify-auth)

## Running Locally

Make sure you have Python [installed properly](http://install.python-guide.org).  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/). Install Postgresql too.

```sh
  Setup repos and Shopify config
$ git clone https://markrmark@bitbucket.org/markrmark/django-shopify-embed.git
$ cd django-shopify-auth
$ pip install -r requirements.txt
$ export SHOPIFY_API_KEY=my_api_key
$ export SHOPIFY_API_SECRET=my_api_secret

  Make sure postgresql is started and configure
$ createdb shopify_sd_app
$ psql
  And enter these commands:
  CREATE USER your_shell_username;
  GRANT ALL PRIVILEGES ON DATABASE shopify_sd_app to your_shell_username;

  Setup and start app
$ python manage.py syncdb
$ gunicorn gettingstarted.wsgi --log-file -
```

Your app should now be running on [localhost:8000](http://localhost:8000/).

You will have to click on the shield icon in your browser bar to allow unsafe scripts, as mixed protocol rules prohibit https sites from embedding http iframes. 

## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master
$ git config:set SHOPIFY_API_KEY=my_api_key
$ git config:set SHOPIFY_API_SECRET=my_api_secret
$ heroku run python manage.py syncdb
$ heroku open
```

Make sure to set your Shopify callback URL to the https protocol version of your Heroku app. Otherwise, the browser will refuse to load the embedded app in the iframe, because of mixed-content rules.

## Documentation

For more information about using Python on Heroku, see these Dev Center articles:

- [Python on Heroku](https://devcenter.heroku.com/categories/python)

