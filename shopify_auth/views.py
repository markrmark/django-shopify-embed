import shopify
from django.http.response import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render, resolve_url
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib import auth
from django.template import RequestContext
from .decorators import anonymous_required


def get_return_address(request):
    return request.REQUEST.get(auth.REDIRECT_FIELD_NAME) or resolve_url(settings.LOGIN_REDIRECT_URL)


@anonymous_required
def login(request, *args, **kwargs):
    print "go login"
    shop = request.REQUEST.get('shop')

    # If the shop parameter has already been provided, attempt to authenticate immediately.
    if shop:
        print "login - already authenticated: " + shop
        return authenticate(request, *args, **kwargs)

    print "login - not authenticated"
    return render(request, "shopify_auth/login.html", context_instance = RequestContext(request, {
        'SHOPIFY_APP_NAME': settings.SHOPIFY_APP_NAME
    }))


@anonymous_required
def authenticate(request, *args, **kwargs):
    print "go authenticate"
    shop = request.REQUEST.get('shop')

    if settings.SHOPIFY_APP_DEV_MODE:
        return finalize(request, token = '00000000000000000000000000000000', *args, **kwargs)

    if shop:
        redirect_uri = request.build_absolute_uri(reverse('shopify_auth.views.finalize'))
        scope = settings.SHOPIFY_APP_API_SCOPE
        permission_url = shopify.Session(shop.strip()).create_permission_url(scope, redirect_uri)
        print "perm url : " + permission_url

        if settings.SHOPIFY_APP_IS_EMBEDDED:
            print "embedded"
            # Embedded Apps should use a Javascript redirect.
            return render(request, "shopify_auth/iframe_redirect.html", {
                'redirect_uri': permission_url
            })
        else:
            # Non-Embedded Apps should use a standard redirect.
            return HttpResponseRedirect(permission_url)

    return_address = get_return_address(request)
    return HttpResponseRedirect(return_address)


@anonymous_required
def finalize(request, *args, **kwargs):
    shop = request.REQUEST.get('shop')
    print "go finalize: " + request.get_full_path()

    try:
        shopify_session = shopify.Session(shop, token = kwargs.get('token'))
        shopify_session.request_token(request.REQUEST)
    except:
        print "no session"
        login_url = reverse('shopify_auth.views.login')
        return HttpResponseRedirect(login_url)

    # Attempt to authenticate the user and log them in.
    print "got token: " + shopify_session.token

    user = auth.authenticate(myshopify_domain = shopify_session.url, token = shopify_session.token)
    if user:
        print "got user"
        auth.login(request, user)

    print "activate session"
    shopify.ShopifyResource.activate_session(shopify_session)

    return_address = get_return_address(request)
    print "finalize and return to: " + return_address
    # print " req user: " + request.user
    return HttpResponseRedirect(return_address)
